# pelim4

He anat afegint estils bàsicament a tota la pàgina. Principalment he fet un menú  amb la propietat hover i linkant-lo amb la part del html que corresponia. 
Dins d'aquest he fet un petit submenú desplegable tot i que no ho he fet amb la propietat dropdown menú ho he pogut fer.
També he buscat un parell de fonts de google i les he incorporat. No he introduit cap fotografia com a fons ja que no m'agradava com quedava i no perquè
no ho sapigués fer. 
Principalment en els meus divs els he introduit en diferents borders perquè sigués més maco visualment i ajustant-los amb els paddings i els margins.
També ho he fet amb algunes fotografies i el video, ajustant bé les mides perquè quedés tot centrat.
També he afegit un header i ha dins hi he posat el h1 així es veia millor.
Més o menys he anat jugant amb els paràmtres apresos com el float, el background-color, les propietats de les fonts, les mides (height i width), el margin i
el padding, el text-align per centrar el contingut, els borders i els border-radius, per donar una mica de forma al contorn i algun position relative i
absolute.

